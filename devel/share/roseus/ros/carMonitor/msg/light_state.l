;; Auto-generated. Do not edit!


(when (boundp 'carMonitor::light_state)
  (if (not (find-package "CARMONITOR"))
    (make-package "CARMONITOR"))
  (shadow 'light_state (find-package "CARMONITOR")))
(unless (find-package "CARMONITOR::LIGHT_STATE")
  (make-package "CARMONITOR::LIGHT_STATE"))

(in-package "ROS")
;;//! \htmlinclude light_state.msg.html


(defclass carMonitor::light_state
  :super ros::object
  :slots (_mode ))

(defmethod carMonitor::light_state
  (:init
   (&key
    ((:mode __mode) 0)
    )
   (send-super :init)
   (setq _mode (round __mode))
   self)
  (:mode
   (&optional __mode)
   (if __mode (setq _mode __mode)) _mode)
  (:serialization-length
   ()
   (+
    ;; int32 _mode
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _mode
       (write-long _mode s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _mode
     (setq _mode (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get carMonitor::light_state :md5sum-) "ff63f6ea3c3e9b7504b2cb3ee0a09d92")
(setf (get carMonitor::light_state :datatype-) "carMonitor/light_state")
(setf (get carMonitor::light_state :definition-)
      "int32 mode
")



(provide :carMonitor/light_state "ff63f6ea3c3e9b7504b2cb3ee0a09d92")


