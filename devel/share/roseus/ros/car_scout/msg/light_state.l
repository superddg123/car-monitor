;; Auto-generated. Do not edit!


(when (boundp 'car_scout::light_state)
  (if (not (find-package "CAR_SCOUT"))
    (make-package "CAR_SCOUT"))
  (shadow 'light_state (find-package "CAR_SCOUT")))
(unless (find-package "CAR_SCOUT::LIGHT_STATE")
  (make-package "CAR_SCOUT::LIGHT_STATE"))

(in-package "ROS")
;;//! \htmlinclude light_state.msg.html


(defclass car_scout::light_state
  :super ros::object
  :slots (_mode ))

(defmethod car_scout::light_state
  (:init
   (&key
    ((:mode __mode) 0)
    )
   (send-super :init)
   (setq _mode (round __mode))
   self)
  (:mode
   (&optional __mode)
   (if __mode (setq _mode __mode)) _mode)
  (:serialization-length
   ()
   (+
    ;; int32 _mode
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _mode
       (write-long _mode s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _mode
     (setq _mode (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get car_scout::light_state :md5sum-) "ff63f6ea3c3e9b7504b2cb3ee0a09d92")
(setf (get car_scout::light_state :datatype-) "car_scout/light_state")
(setf (get car_scout::light_state :definition-)
      "int32 mode
")



(provide :car_scout/light_state "ff63f6ea3c3e9b7504b2cb3ee0a09d92")


