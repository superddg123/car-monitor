#pragma once
#include <ros/ros.h>
#include <carMonitor/car_msg.h>

struct motor{
        float current;
        float rpm;
        float temper;
        float voltage;
        float driverTemper;
    };

struct carStatus{
    bool check=false;//检查是否有数据
    float linear;
    float voltage;
    int light;
    int direct;//1 2 3 4 上下左右
    motor motor1;//前左
    motor motor2;//前右
    motor motor3;//后左
    motor motor4;//后右

};


class carSubscriber{
public:
    carSubscriber(ros::NodeHandle _nh);
    void carInfoCallback(const carMonitor::car_msg::ConstPtr &msg);//订阅者回调函数
    carStatus getCarInfo();
    

private:
    ros::NodeHandle nh;
    ros::Subscriber subscriber;
    carStatus car;
};


