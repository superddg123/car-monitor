(cl:in-package car_scout-msg)
(cl:export '(STAMP-VAL
          STAMP
          CONTROL_MODE-VAL
          CONTROL_MODE
          BATTERY_VOLTAGE-VAL
          BATTERY_VOLTAGE
          LIGHT_CONTROL_ENABLED-VAL
          LIGHT_CONTROL_ENABLED
          FRONT_LIGHT_STATE-VAL
          FRONT_LIGHT_STATE
          REAR_LIGHT_STATE-VAL
          REAR_LIGHT_STATE
          MOTOR_NUM-VAL
          MOTOR_NUM
          MOTORS-VAL
          MOTORS
          MOTION-VAL
          MOTION
))