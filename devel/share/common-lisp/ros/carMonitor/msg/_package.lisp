(cl:defpackage carMonitor-msg
  (:use )
  (:export
   "<CAR_MSG>"
   "CAR_MSG"
   "<LIGHT_STATE>"
   "LIGHT_STATE"
   "<MOTION_STATE>"
   "MOTION_STATE"
   "<MOTOR_STATE>"
   "MOTOR_STATE"
  ))

