; Auto-generated. Do not edit!


(cl:in-package carMonitor-msg)


;//! \htmlinclude car_msg.msg.html

(cl:defclass <car_msg> (roslisp-msg-protocol:ros-message)
  ((stamp
    :reader stamp
    :initarg :stamp
    :type cl:real
    :initform 0)
   (control_mode
    :reader control_mode
    :initarg :control_mode
    :type cl:integer
    :initform 0)
   (battery_voltage
    :reader battery_voltage
    :initarg :battery_voltage
    :type cl:float
    :initform 0.0)
   (light_control_enabled
    :reader light_control_enabled
    :initarg :light_control_enabled
    :type cl:boolean
    :initform cl:nil)
   (front_light_state
    :reader front_light_state
    :initarg :front_light_state
    :type carMonitor-msg:light_state
    :initform (cl:make-instance 'carMonitor-msg:light_state))
   (rear_light_state
    :reader rear_light_state
    :initarg :rear_light_state
    :type carMonitor-msg:light_state
    :initform (cl:make-instance 'carMonitor-msg:light_state))
   (motor_num
    :reader motor_num
    :initarg :motor_num
    :type cl:integer
    :initform 0)
   (motors
    :reader motors
    :initarg :motors
    :type (cl:vector carMonitor-msg:motor_state)
   :initform (cl:make-array 0 :element-type 'carMonitor-msg:motor_state :initial-element (cl:make-instance 'carMonitor-msg:motor_state)))
   (motion
    :reader motion
    :initarg :motion
    :type carMonitor-msg:motion_state
    :initform (cl:make-instance 'carMonitor-msg:motion_state)))
)

(cl:defclass car_msg (<car_msg>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <car_msg>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'car_msg)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name carMonitor-msg:<car_msg> is deprecated: use carMonitor-msg:car_msg instead.")))

(cl:ensure-generic-function 'stamp-val :lambda-list '(m))
(cl:defmethod stamp-val ((m <car_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader carMonitor-msg:stamp-val is deprecated.  Use carMonitor-msg:stamp instead.")
  (stamp m))

(cl:ensure-generic-function 'control_mode-val :lambda-list '(m))
(cl:defmethod control_mode-val ((m <car_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader carMonitor-msg:control_mode-val is deprecated.  Use carMonitor-msg:control_mode instead.")
  (control_mode m))

(cl:ensure-generic-function 'battery_voltage-val :lambda-list '(m))
(cl:defmethod battery_voltage-val ((m <car_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader carMonitor-msg:battery_voltage-val is deprecated.  Use carMonitor-msg:battery_voltage instead.")
  (battery_voltage m))

(cl:ensure-generic-function 'light_control_enabled-val :lambda-list '(m))
(cl:defmethod light_control_enabled-val ((m <car_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader carMonitor-msg:light_control_enabled-val is deprecated.  Use carMonitor-msg:light_control_enabled instead.")
  (light_control_enabled m))

(cl:ensure-generic-function 'front_light_state-val :lambda-list '(m))
(cl:defmethod front_light_state-val ((m <car_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader carMonitor-msg:front_light_state-val is deprecated.  Use carMonitor-msg:front_light_state instead.")
  (front_light_state m))

(cl:ensure-generic-function 'rear_light_state-val :lambda-list '(m))
(cl:defmethod rear_light_state-val ((m <car_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader carMonitor-msg:rear_light_state-val is deprecated.  Use carMonitor-msg:rear_light_state instead.")
  (rear_light_state m))

(cl:ensure-generic-function 'motor_num-val :lambda-list '(m))
(cl:defmethod motor_num-val ((m <car_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader carMonitor-msg:motor_num-val is deprecated.  Use carMonitor-msg:motor_num instead.")
  (motor_num m))

(cl:ensure-generic-function 'motors-val :lambda-list '(m))
(cl:defmethod motors-val ((m <car_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader carMonitor-msg:motors-val is deprecated.  Use carMonitor-msg:motors instead.")
  (motors m))

(cl:ensure-generic-function 'motion-val :lambda-list '(m))
(cl:defmethod motion-val ((m <car_msg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader carMonitor-msg:motion-val is deprecated.  Use carMonitor-msg:motion instead.")
  (motion m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <car_msg>) ostream)
  "Serializes a message object of type '<car_msg>"
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'stamp)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'stamp) (cl:floor (cl:slot-value msg 'stamp)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
  (cl:let* ((signed (cl:slot-value msg 'control_mode)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'battery_voltage))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'light_control_enabled) 1 0)) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'front_light_state) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'rear_light_state) ostream)
  (cl:let* ((signed (cl:slot-value msg 'motor_num)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'motors))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'motors))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'motion) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <car_msg>) istream)
  "Deserializes a message object of type '<car_msg>"
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'stamp) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'control_mode) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'battery_voltage) (roslisp-utils:decode-double-float-bits bits)))
    (cl:setf (cl:slot-value msg 'light_control_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'front_light_state) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'rear_light_state) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'motor_num) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'motors) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'motors)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'carMonitor-msg:motor_state))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'motion) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<car_msg>)))
  "Returns string type for a message object of type '<car_msg>"
  "carMonitor/car_msg")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'car_msg)))
  "Returns string type for a message object of type 'car_msg"
  "carMonitor/car_msg")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<car_msg>)))
  "Returns md5sum for a message object of type '<car_msg>"
  "e9d4a19a36701fd76b3d661468664a5a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'car_msg)))
  "Returns md5sum for a message object of type 'car_msg"
  "e9d4a19a36701fd76b3d661468664a5a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<car_msg>)))
  "Returns full string definition for message of type '<car_msg>"
  (cl:format cl:nil "time stamp~%int32 control_mode~%float64 battery_voltage~%bool light_control_enabled~%~%light_state front_light_state~%light_state rear_light_state~%~%int32 motor_num~%motor_state[] motors~%~%motion_state motion~%~%================================================================================~%MSG: carMonitor/light_state~%int32 mode~%================================================================================~%MSG: carMonitor/motor_state~%float64 motor_current ~%float64 motor_rpm   ~%int32 motor_pulses   ~%float64 motor_temperature   ~%~%float64 driver_voltage   ~%float64 driver_temperature   ~%int32 driver_state   ~%================================================================================~%MSG: carMonitor/motion_state~%  float64 linear_velocity~%  float64 angular_velocity ~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'car_msg)))
  "Returns full string definition for message of type 'car_msg"
  (cl:format cl:nil "time stamp~%int32 control_mode~%float64 battery_voltage~%bool light_control_enabled~%~%light_state front_light_state~%light_state rear_light_state~%~%int32 motor_num~%motor_state[] motors~%~%motion_state motion~%~%================================================================================~%MSG: carMonitor/light_state~%int32 mode~%================================================================================~%MSG: carMonitor/motor_state~%float64 motor_current ~%float64 motor_rpm   ~%int32 motor_pulses   ~%float64 motor_temperature   ~%~%float64 driver_voltage   ~%float64 driver_temperature   ~%int32 driver_state   ~%================================================================================~%MSG: carMonitor/motion_state~%  float64 linear_velocity~%  float64 angular_velocity ~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <car_msg>))
  (cl:+ 0
     8
     4
     8
     1
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'front_light_state))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'rear_light_state))
     4
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'motors) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'motion))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <car_msg>))
  "Converts a ROS message object to a list"
  (cl:list 'car_msg
    (cl:cons ':stamp (stamp msg))
    (cl:cons ':control_mode (control_mode msg))
    (cl:cons ':battery_voltage (battery_voltage msg))
    (cl:cons ':light_control_enabled (light_control_enabled msg))
    (cl:cons ':front_light_state (front_light_state msg))
    (cl:cons ':rear_light_state (rear_light_state msg))
    (cl:cons ':motor_num (motor_num msg))
    (cl:cons ':motors (motors msg))
    (cl:cons ':motion (motion msg))
))
