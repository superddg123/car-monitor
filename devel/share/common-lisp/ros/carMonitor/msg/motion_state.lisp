; Auto-generated. Do not edit!


(cl:in-package carMonitor-msg)


;//! \htmlinclude motion_state.msg.html

(cl:defclass <motion_state> (roslisp-msg-protocol:ros-message)
  ((linear_velocity
    :reader linear_velocity
    :initarg :linear_velocity
    :type cl:float
    :initform 0.0)
   (angular_velocity
    :reader angular_velocity
    :initarg :angular_velocity
    :type cl:float
    :initform 0.0))
)

(cl:defclass motion_state (<motion_state>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <motion_state>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'motion_state)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name carMonitor-msg:<motion_state> is deprecated: use carMonitor-msg:motion_state instead.")))

(cl:ensure-generic-function 'linear_velocity-val :lambda-list '(m))
(cl:defmethod linear_velocity-val ((m <motion_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader carMonitor-msg:linear_velocity-val is deprecated.  Use carMonitor-msg:linear_velocity instead.")
  (linear_velocity m))

(cl:ensure-generic-function 'angular_velocity-val :lambda-list '(m))
(cl:defmethod angular_velocity-val ((m <motion_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader carMonitor-msg:angular_velocity-val is deprecated.  Use carMonitor-msg:angular_velocity instead.")
  (angular_velocity m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <motion_state>) ostream)
  "Serializes a message object of type '<motion_state>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'linear_velocity))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'angular_velocity))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <motion_state>) istream)
  "Deserializes a message object of type '<motion_state>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'linear_velocity) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'angular_velocity) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<motion_state>)))
  "Returns string type for a message object of type '<motion_state>"
  "carMonitor/motion_state")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'motion_state)))
  "Returns string type for a message object of type 'motion_state"
  "carMonitor/motion_state")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<motion_state>)))
  "Returns md5sum for a message object of type '<motion_state>"
  "e55b2cec3678035367208627e07de350")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'motion_state)))
  "Returns md5sum for a message object of type 'motion_state"
  "e55b2cec3678035367208627e07de350")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<motion_state>)))
  "Returns full string definition for message of type '<motion_state>"
  (cl:format cl:nil "  float64 linear_velocity~%  float64 angular_velocity ~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'motion_state)))
  "Returns full string definition for message of type 'motion_state"
  (cl:format cl:nil "  float64 linear_velocity~%  float64 angular_velocity ~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <motion_state>))
  (cl:+ 0
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <motion_state>))
  "Converts a ROS message object to a list"
  (cl:list 'motion_state
    (cl:cons ':linear_velocity (linear_velocity msg))
    (cl:cons ':angular_velocity (angular_velocity msg))
))
