#include "CarMonitor.h"
#include <QApplication>
#include <CarMsgHandle.h>

#pragma comment(lib, "user32.lib")

int main(int argc, char *argv[])
{
    ros::init(argc, argv,"carMonitor");
    ros::NodeHandle carNh;
    QApplication a(argc, argv);
    
    carSubscriber carSub(carNh);//创建小车订阅对象
    //避免ROS和Qt事件循环之间的冲突,使用异步事件循环机制
    ros::AsyncSpinner spinner(1);  // 在一个线程中运行ROS事件循环
    spinner.start();

    carMonitorWin app(nullptr,carSub,carNh);
    app.show();
    app.startMonitor();
    int result=a.exec();

    spinner.stop();
    return result;
}