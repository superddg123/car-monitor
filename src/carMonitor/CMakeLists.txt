cmake_minimum_required(VERSION 3.0.2)
project(carMonitor)

#ros需要的配置----------------------------------------
find_package(catkin REQUIRED COMPONENTS
  roscpp
  message_generation
  std_msgs
)


#message("catkin头文件路径" ${catkin_INCLUDE_DIRS})
#生成自定义消息文件
add_message_files(
  FILES
  car_msg.msg
  light_state.msg
  motion_state.msg
  motor_state.msg
  )

generate_messages(DEPENDENCIES std_msgs)

catkin_package()

include_directories(
  ${catkin_INCLUDE_DIRS}
  include/carMonitor/ #生成的消息头文件路径
)
#qt5需要的配置----------------------------------------
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_PREFIX_PATH "/usr/lib/x86_64-linux-gnu/qt5") # Qt Kit Dir
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
find_package(Qt5 COMPONENTS Widgets REQUIRED) # Qt COMPONENTS
include_directories("include")#设置头文件目录
aux_source_directory(./src srcs)#添加源文件目录.cpp .c
FILE(GLOB SOURCES_FILES "source/*.qrc")#增加qrc文件
FILE(GLOB HEADER_FILES "include/*.h")#增加头文件
#FILE(GLOB UI_FILES "src/gui/*.ui") 
#set(UI_FILES "src/UI/*.ui")
#qt5_wrap_ui(UI_HEADER ${UI_FILES}) # 自动生成 ui_*.h 文件

# 添加资源文件
message("当前路径：${CMAKE_CURRENT_SOURCE_DIR}")
message("资源路径：" ${SOURCES_FILES})
message("项目名：" ${PROJECT_NAME})
message("头文件：" ${HEADER_FILES} )
# 【optional】生成UI文件夹
#SOURCE_GROUP("UI" FILES ${UI_FILES} ${UI_HEADER} ${HEADER_FILES})
# Specify MSVC UTF-8 encoding   
add_compile_options("$<$<C_COMPILER_ID:MSVC>:/utf-8>")
add_compile_options("$<$<CXX_COMPILER_ID:MSVC>:/utf-8>")

#公共部分----------------------------------
add_executable(${PROJECT_NAME}_node 
  WIN32 # If you need a terminal for debug, please comment this statement 
  ${srcs} 
  ${HEADER_FILES}
  ${SOURCES_FILES}

)

target_link_libraries(${PROJECT_NAME}_node PRIVATE
  ${catkin_LIBRARIES}
  Qt5::Widgets
  
)