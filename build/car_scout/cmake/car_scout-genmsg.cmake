# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "car_scout: 4 messages, 0 services")

set(MSG_I_FLAGS "-Icar_scout:/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(car_scout_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg" NAME_WE)
add_custom_target(_car_scout_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "car_scout" "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg" ""
)

get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg" NAME_WE)
add_custom_target(_car_scout_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "car_scout" "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg" "car_scout/motor_state:car_scout/light_state:car_scout/motion_state"
)

get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg" NAME_WE)
add_custom_target(_car_scout_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "car_scout" "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg" ""
)

get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg" NAME_WE)
add_custom_target(_car_scout_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "car_scout" "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/car_scout
)
_generate_msg_cpp(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/car_scout
)
_generate_msg_cpp(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/car_scout
)
_generate_msg_cpp(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg"
  "${MSG_I_FLAGS}"
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/car_scout
)

### Generating Services

### Generating Module File
_generate_module_cpp(car_scout
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/car_scout
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(car_scout_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(car_scout_generate_messages car_scout_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_cpp _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_cpp _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_cpp _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_cpp _car_scout_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(car_scout_gencpp)
add_dependencies(car_scout_gencpp car_scout_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS car_scout_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/car_scout
)
_generate_msg_eus(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/car_scout
)
_generate_msg_eus(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/car_scout
)
_generate_msg_eus(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg"
  "${MSG_I_FLAGS}"
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/car_scout
)

### Generating Services

### Generating Module File
_generate_module_eus(car_scout
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/car_scout
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(car_scout_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(car_scout_generate_messages car_scout_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_eus _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_eus _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_eus _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_eus _car_scout_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(car_scout_geneus)
add_dependencies(car_scout_geneus car_scout_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS car_scout_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/car_scout
)
_generate_msg_lisp(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/car_scout
)
_generate_msg_lisp(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/car_scout
)
_generate_msg_lisp(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg"
  "${MSG_I_FLAGS}"
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/car_scout
)

### Generating Services

### Generating Module File
_generate_module_lisp(car_scout
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/car_scout
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(car_scout_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(car_scout_generate_messages car_scout_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_lisp _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_lisp _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_lisp _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_lisp _car_scout_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(car_scout_genlisp)
add_dependencies(car_scout_genlisp car_scout_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS car_scout_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/car_scout
)
_generate_msg_nodejs(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/car_scout
)
_generate_msg_nodejs(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/car_scout
)
_generate_msg_nodejs(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg"
  "${MSG_I_FLAGS}"
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/car_scout
)

### Generating Services

### Generating Module File
_generate_module_nodejs(car_scout
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/car_scout
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(car_scout_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(car_scout_generate_messages car_scout_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_nodejs _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_nodejs _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_nodejs _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_nodejs _car_scout_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(car_scout_gennodejs)
add_dependencies(car_scout_gennodejs car_scout_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS car_scout_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/car_scout
)
_generate_msg_py(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/car_scout
)
_generate_msg_py(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/car_scout
)
_generate_msg_py(car_scout
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg"
  "${MSG_I_FLAGS}"
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/car_scout
)

### Generating Services

### Generating Module File
_generate_module_py(car_scout
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/car_scout
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(car_scout_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(car_scout_generate_messages car_scout_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motion_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_py _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/car_msg.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_py _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/motor_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_py _car_scout_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/msg/light_state.msg" NAME_WE)
add_dependencies(car_scout_generate_messages_py _car_scout_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(car_scout_genpy)
add_dependencies(car_scout_genpy car_scout_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS car_scout_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/car_scout)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/car_scout
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(car_scout_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/car_scout)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/car_scout
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(car_scout_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/car_scout)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/car_scout
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(car_scout_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/car_scout)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/car_scout
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(car_scout_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/car_scout)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/car_scout\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/car_scout
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(car_scout_generate_messages_py std_msgs_generate_messages_py)
endif()
