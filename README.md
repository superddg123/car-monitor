# carMonitor

#### 介绍
松灵小车任务3,读取小车车灯、运动状态、电机状态并显示。在rviz中查看小车位置和运动轨迹

![输入图片说明](carMonitor.png)

#### 使用说明

启动小车信息读取、运动控制、qt界面三个节点
```
roslaunch car_scout carinfo.launch
```


