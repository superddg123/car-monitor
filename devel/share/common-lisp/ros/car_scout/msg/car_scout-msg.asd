
(cl:in-package :asdf)

(defsystem "car_scout-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "car_msg" :depends-on ("_package_car_msg"))
    (:file "_package_car_msg" :depends-on ("_package"))
    (:file "light_state" :depends-on ("_package_light_state"))
    (:file "_package_light_state" :depends-on ("_package"))
    (:file "motion_state" :depends-on ("_package_motion_state"))
    (:file "_package_motion_state" :depends-on ("_package"))
    (:file "motor_state" :depends-on ("_package_motor_state"))
    (:file "_package_motor_state" :depends-on ("_package"))
  ))