#include<ros/ros.h>
#include<car_scout/motion_state.h>
#include "ugv_sdk/scout/scout_base.hpp"
using namespace westonrobot;

ScoutBase scout;


void controlCallback(const car_scout::motion_state::ConstPtr &msg){
        double linear_velocity=msg->linear_velocity;
        double angular_velocity=msg->angular_velocity;
        ROS_INFO("received:linear:%.2f,angular:%.2f",linear_velocity,angular_velocity);
        scout.SetMotionCommand(linear_velocity, angular_velocity);
}

int main(int argc, char **argv){
    std::string device_name="can0";
    int32_t baud_rate = 0;
  
    scout.Connect(device_name, baud_rate);
    scout.EnableCommandedMode();
    ros::init(argc,argv,"soft_control");
    ros::NodeHandle nh;
    ros::Subscriber carSub=nh.subscribe("/car_control",1,controlCallback);

    ros::spin();
    scout.Disconnect();
    scout.Terminate();
    return 0;
}


