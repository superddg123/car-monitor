/****************************************************************************
** Meta object code from reading C++ file 'CarMonitor.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/carMonitor/include/CarMonitor.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CarMonitor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_carMonitorWin_t {
    QByteArrayData data[8];
    char stringdata0[99];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_carMonitorWin_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_carMonitorWin_t qt_meta_stringdata_carMonitorWin = {
    {
QT_MOC_LITERAL(0, 0, 13), // "carMonitorWin"
QT_MOC_LITERAL(1, 14, 12), // "directSignal"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 6), // "direct"
QT_MOC_LITERAL(4, 35, 11), // "button_slot"
QT_MOC_LITERAL(5, 47, 11), // "direct_slot"
QT_MOC_LITERAL(6, 59, 22), // "buttonReleased_Bg_slot"
QT_MOC_LITERAL(7, 82, 16) // "motionTimer_slot"

    },
    "carMonitorWin\0directSignal\0\0direct\0"
    "button_slot\0direct_slot\0buttonReleased_Bg_slot\0"
    "motionTimer_slot"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_carMonitorWin[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   42,    2, 0x0a /* Public */,
       5,    1,   43,    2, 0x0a /* Public */,
       6,    0,   46,    2, 0x0a /* Public */,
       7,    0,   47,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void carMonitorWin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        carMonitorWin *_t = static_cast<carMonitorWin *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->directSignal((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->button_slot(); break;
        case 2: _t->direct_slot((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->buttonReleased_Bg_slot(); break;
        case 4: _t->motionTimer_slot(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (carMonitorWin::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&carMonitorWin::directSignal)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject carMonitorWin::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_carMonitorWin.data,
      qt_meta_data_carMonitorWin,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *carMonitorWin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *carMonitorWin::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_carMonitorWin.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int carMonitorWin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void carMonitorWin::directSignal(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
