#include"CarMsgHandle.h"
carSubscriber::carSubscriber(ros::NodeHandle _nh){
    nh=_nh;
    subscriber=nh.subscribe("/car_info",2,&carSubscriber::carInfoCallback,this);
    ROS_INFO("订阅者初始化");
}

void carSubscriber::carInfoCallback(const carMonitor::car_msg::ConstPtr &msg){
    car.check=false;
    car.linear=msg->motion.linear_velocity;
    car.voltage=msg->battery_voltage;
    car.light=msg->front_light_state.mode;

    car.motor1.current=msg->motors[0].motor_current;
    car.motor1.voltage=msg->motors[0].driver_voltage;
    car.motor1.rpm=msg->motors[0].motor_rpm;
    car.motor1.temper=msg->motors[0].motor_temperature;
    car.motor1.driverTemper=msg->motors[0].driver_temperature;

    car.motor2.current=msg->motors[1].motor_current;
    car.motor2.voltage=msg->motors[1].driver_voltage;
    car.motor2.rpm=msg->motors[1].motor_rpm;
    car.motor2.temper=msg->motors[1].motor_temperature;
    car.motor2.driverTemper=msg->motors[1].driver_temperature;

    car.motor3.current=msg->motors[2].motor_current;
    car.motor3.voltage=msg->motors[2].driver_voltage;
    car.motor3.rpm=msg->motors[2].motor_rpm;
    car.motor3.temper=msg->motors[2].motor_temperature;
    car.motor3.driverTemper=msg->motors[2].driver_temperature;

    car.motor4.current=msg->motors[3].motor_current;
    car.motor4.voltage=msg->motors[3].driver_voltage;
    car.motor4.rpm=msg->motors[3].motor_rpm;
    car.motor4.temper=msg->motors[3].motor_temperature;
    car.motor4.driverTemper=msg->motors[3].driver_temperature;
    car.check=true;
    ROS_INFO("getCarInfo volatge:%.2f  ,   linear:%.2f",car.voltage,car.linear);
}

carStatus carSubscriber::getCarInfo(){
    return car;
}



