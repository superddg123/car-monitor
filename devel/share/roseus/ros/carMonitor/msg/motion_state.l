;; Auto-generated. Do not edit!


(when (boundp 'carMonitor::motion_state)
  (if (not (find-package "CARMONITOR"))
    (make-package "CARMONITOR"))
  (shadow 'motion_state (find-package "CARMONITOR")))
(unless (find-package "CARMONITOR::MOTION_STATE")
  (make-package "CARMONITOR::MOTION_STATE"))

(in-package "ROS")
;;//! \htmlinclude motion_state.msg.html


(defclass carMonitor::motion_state
  :super ros::object
  :slots (_linear_velocity _angular_velocity ))

(defmethod carMonitor::motion_state
  (:init
   (&key
    ((:linear_velocity __linear_velocity) 0.0)
    ((:angular_velocity __angular_velocity) 0.0)
    )
   (send-super :init)
   (setq _linear_velocity (float __linear_velocity))
   (setq _angular_velocity (float __angular_velocity))
   self)
  (:linear_velocity
   (&optional __linear_velocity)
   (if __linear_velocity (setq _linear_velocity __linear_velocity)) _linear_velocity)
  (:angular_velocity
   (&optional __angular_velocity)
   (if __angular_velocity (setq _angular_velocity __angular_velocity)) _angular_velocity)
  (:serialization-length
   ()
   (+
    ;; float64 _linear_velocity
    8
    ;; float64 _angular_velocity
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _linear_velocity
       (sys::poke _linear_velocity (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _angular_velocity
       (sys::poke _angular_velocity (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _linear_velocity
     (setq _linear_velocity (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _angular_velocity
     (setq _angular_velocity (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get carMonitor::motion_state :md5sum-) "e55b2cec3678035367208627e07de350")
(setf (get carMonitor::motion_state :datatype-) "carMonitor/motion_state")
(setf (get carMonitor::motion_state :definition-)
      "  float64 linear_velocity
  float64 angular_velocity 
")



(provide :carMonitor/motion_state "e55b2cec3678035367208627e07de350")


