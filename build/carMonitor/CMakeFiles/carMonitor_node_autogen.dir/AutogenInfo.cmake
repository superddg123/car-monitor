# Meta
set(AM_MULTI_CONFIG "SINGLE")
# Directories and files
set(AM_CMAKE_BINARY_DIR "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/build/")
set(AM_CMAKE_SOURCE_DIR "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/")
set(AM_CMAKE_CURRENT_SOURCE_DIR "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/")
set(AM_CMAKE_CURRENT_BINARY_DIR "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/build/carMonitor/")
set(AM_CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE "")
set(AM_BUILD_DIR "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/build/carMonitor/carMonitor_node_autogen")
set(AM_SOURCES "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/src/CarMonitor.cpp;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/src/CarMsgHandle.cpp;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/src/main.cpp")
set(AM_HEADERS "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/include/CarMonitor.h;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/include/CarMsgHandle.h")
# Qt environment
set(AM_QT_VERSION_MAJOR "5")
set(AM_QT_VERSION_MINOR "9")
set(AM_QT_MOC_EXECUTABLE "/usr/lib/qt5/bin/moc")
set(AM_QT_UIC_EXECUTABLE "/usr/lib/qt5/bin/uic")
set(AM_QT_RCC_EXECUTABLE "/usr/lib/qt5/bin/rcc")
# MOC settings
set(AM_MOC_SKIP "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/build/carMonitor/carMonitor_node_autogen/CAWN4ZDDTK/qrc_resource.cpp;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/build/carMonitor/carMonitor_node_autogen/mocs_compilation.cpp")
set(AM_MOC_DEFINITIONS "QT_CORE_LIB;QT_GUI_LIB;QT_NO_DEBUG;QT_WIDGETS_LIB;ROSCONSOLE_BACKEND_LOG4CXX;ROS_BUILD_SHARED_LIBS=1;ROS_PACKAGE_NAME=\"carMonitor\"")
set(AM_MOC_INCLUDES "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/build/carMonitor;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/build/carMonitor/carMonitor_node_autogen/include;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/devel/include;/opt/ros/melodic/include;/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/include/carMonitor;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/include;/usr/include/aarch64-linux-gnu/qt5;/usr/include/aarch64-linux-gnu/qt5/QtWidgets;/usr/include/aarch64-linux-gnu/qt5/QtGui;/usr/include/aarch64-linux-gnu/qt5/QtCore;/usr/lib/aarch64-linux-gnu/qt5/mkspecs/linux-g++;/usr/include")
set(AM_MOC_OPTIONS "")
set(AM_MOC_RELAXED_MODE "FALSE")
set(AM_MOC_MACRO_NAMES "Q_OBJECT;Q_GADGET;Q_NAMESPACE")
set(AM_MOC_DEPEND_FILTERS "")
set(AM_MOC_PREDEFS_CMD "/usr/bin/c++;-dM;-E;-c;/usr/share/cmake-3.10/Modules/CMakeCXXCompilerABI.cpp")
# UIC settings
set(AM_UIC_SKIP "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/build/carMonitor/carMonitor_node_autogen/CAWN4ZDDTK/qrc_resource.cpp;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/build/carMonitor/carMonitor_node_autogen/mocs_compilation.cpp")
set(AM_UIC_TARGET_OPTIONS "")
set(AM_UIC_OPTIONS_FILES "")
set(AM_UIC_OPTIONS_OPTIONS "")
set(AM_UIC_SEARCH_PATHS "")
# RCC settings
set(AM_RCC_SOURCES "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/source/resource.qrc")
set(AM_RCC_BUILDS "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/build/carMonitor/carMonitor_node_autogen/CAWN4ZDDTK/qrc_resource.cpp")
set(AM_RCC_OPTIONS "{-name;resource}")
set(AM_RCC_INPUTS "{/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/source/img/向下4.png;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/source/img/充电.png;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/source/img/向左.png;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/source/img/速度.png;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/source/img/向上4.png;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/source/img/carModel.png;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/source/img/车灯.png;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/source/img/向右.png;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/source/img/电机.png}")
# Configurations options
set(AM_CONFIG_SUFFIX_ "_")
