#pragma once
#include "ui_CarMonitor.h"
#include <QMainWindow>
#include <CarMsgHandle.h>
#include "carMonitor/motion_state.h"
#include <QDebug>
#include <thread>
#include <chrono>
#include <QTimer>
#include<QKeyEvent>
using namespace std;
class carMonitorWin : public QMainWindow {
    Q_OBJECT
    
public:
    carMonitorWin(QWidget* parent,carSubscriber& sub,ros::NodeHandle _nh);
    ~carMonitorWin();
    void display();
    void startMonitor();
    bool runing=false;
public slots:
    void button_slot();//上下左右
    void direct_slot(QString direct);//小车运动方向显示
    // void buttonPressed_Bg_slot();
    void buttonReleased_Bg_slot();
    void motionTimer_slot();
signals:
    void directSignal(QString direct);//自定义信号
protected:
    void closeEvent(QCloseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void carRun(QString  direction);
    void carStop();

private:
    Ui_CarMonitor* ui;
    carSubscriber& carSub;
    ros::Publisher pub;
    carStatus carInfo;
    std::thread threadObj;
    QTimer timer;//用于恢复按钮背景
    QTimer motionTimer;
    QString preDirect="static";//上一个方向
    carMonitor::motion_state msg;//控制消息
    //单位
    QString name_current="电流：";
    QString name_temper="温度：";
    QString name_rpm="转速：";
    QString name_driverTemper="驱动器温度：";
    QString name_driverVoltage="驱动器电压：";
    QString carDirect="static";
};
