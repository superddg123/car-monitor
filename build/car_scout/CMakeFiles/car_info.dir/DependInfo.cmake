# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/src/car_info.cpp" "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/build/car_scout/CMakeFiles/car_info.dir/src/car_info.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"car_scout\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/devel/include"
  "/home/agilex/catkin_ws/src/ugv_sdk/ugv_sdk/include"
  "/home/agilex/catkin_ws/src/ugv_sdk/wrp_io/include"
  "/home/agilex/catkin_ws/src/ugv_sdk/wrp_io/include/asio/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/car_scout/include/car_scout"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
