// Auto-generated. Do not edit!

// (in-package car_scout.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let light_state = require('./light_state.js');
let motor_state = require('./motor_state.js');
let motion_state = require('./motion_state.js');

//-----------------------------------------------------------

class car_msg {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.stamp = null;
      this.control_mode = null;
      this.battery_voltage = null;
      this.light_control_enabled = null;
      this.front_light_state = null;
      this.rear_light_state = null;
      this.motor_num = null;
      this.motors = null;
      this.motion = null;
    }
    else {
      if (initObj.hasOwnProperty('stamp')) {
        this.stamp = initObj.stamp
      }
      else {
        this.stamp = {secs: 0, nsecs: 0};
      }
      if (initObj.hasOwnProperty('control_mode')) {
        this.control_mode = initObj.control_mode
      }
      else {
        this.control_mode = 0;
      }
      if (initObj.hasOwnProperty('battery_voltage')) {
        this.battery_voltage = initObj.battery_voltage
      }
      else {
        this.battery_voltage = 0.0;
      }
      if (initObj.hasOwnProperty('light_control_enabled')) {
        this.light_control_enabled = initObj.light_control_enabled
      }
      else {
        this.light_control_enabled = false;
      }
      if (initObj.hasOwnProperty('front_light_state')) {
        this.front_light_state = initObj.front_light_state
      }
      else {
        this.front_light_state = new light_state();
      }
      if (initObj.hasOwnProperty('rear_light_state')) {
        this.rear_light_state = initObj.rear_light_state
      }
      else {
        this.rear_light_state = new light_state();
      }
      if (initObj.hasOwnProperty('motor_num')) {
        this.motor_num = initObj.motor_num
      }
      else {
        this.motor_num = 0;
      }
      if (initObj.hasOwnProperty('motors')) {
        this.motors = initObj.motors
      }
      else {
        this.motors = [];
      }
      if (initObj.hasOwnProperty('motion')) {
        this.motion = initObj.motion
      }
      else {
        this.motion = new motion_state();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type car_msg
    // Serialize message field [stamp]
    bufferOffset = _serializer.time(obj.stamp, buffer, bufferOffset);
    // Serialize message field [control_mode]
    bufferOffset = _serializer.int32(obj.control_mode, buffer, bufferOffset);
    // Serialize message field [battery_voltage]
    bufferOffset = _serializer.float64(obj.battery_voltage, buffer, bufferOffset);
    // Serialize message field [light_control_enabled]
    bufferOffset = _serializer.bool(obj.light_control_enabled, buffer, bufferOffset);
    // Serialize message field [front_light_state]
    bufferOffset = light_state.serialize(obj.front_light_state, buffer, bufferOffset);
    // Serialize message field [rear_light_state]
    bufferOffset = light_state.serialize(obj.rear_light_state, buffer, bufferOffset);
    // Serialize message field [motor_num]
    bufferOffset = _serializer.int32(obj.motor_num, buffer, bufferOffset);
    // Serialize message field [motors]
    // Serialize the length for message field [motors]
    bufferOffset = _serializer.uint32(obj.motors.length, buffer, bufferOffset);
    obj.motors.forEach((val) => {
      bufferOffset = motor_state.serialize(val, buffer, bufferOffset);
    });
    // Serialize message field [motion]
    bufferOffset = motion_state.serialize(obj.motion, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type car_msg
    let len;
    let data = new car_msg(null);
    // Deserialize message field [stamp]
    data.stamp = _deserializer.time(buffer, bufferOffset);
    // Deserialize message field [control_mode]
    data.control_mode = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [battery_voltage]
    data.battery_voltage = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [light_control_enabled]
    data.light_control_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [front_light_state]
    data.front_light_state = light_state.deserialize(buffer, bufferOffset);
    // Deserialize message field [rear_light_state]
    data.rear_light_state = light_state.deserialize(buffer, bufferOffset);
    // Deserialize message field [motor_num]
    data.motor_num = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [motors]
    // Deserialize array length for message field [motors]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.motors = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.motors[i] = motor_state.deserialize(buffer, bufferOffset)
    }
    // Deserialize message field [motion]
    data.motion = motion_state.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 48 * object.motors.length;
    return length + 53;
  }

  static datatype() {
    // Returns string type for a message object
    return 'car_scout/car_msg';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'e9d4a19a36701fd76b3d661468664a5a';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    time stamp
    int32 control_mode
    float64 battery_voltage
    bool light_control_enabled
    
    light_state front_light_state
    light_state rear_light_state
    
    int32 motor_num
    motor_state[] motors
    
    motion_state motion
    
    ================================================================================
    MSG: car_scout/light_state
    int32 mode
    ================================================================================
    MSG: car_scout/motor_state
    float64 motor_current 
    float64 motor_rpm   
    int32 motor_pulses   
    float64 motor_temperature   
    
    float64 driver_voltage   
    float64 driver_temperature   
    int32 driver_state   
    ================================================================================
    MSG: car_scout/motion_state
      float64 linear_velocity
      float64 angular_velocity 
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new car_msg(null);
    if (msg.stamp !== undefined) {
      resolved.stamp = msg.stamp;
    }
    else {
      resolved.stamp = {secs: 0, nsecs: 0}
    }

    if (msg.control_mode !== undefined) {
      resolved.control_mode = msg.control_mode;
    }
    else {
      resolved.control_mode = 0
    }

    if (msg.battery_voltage !== undefined) {
      resolved.battery_voltage = msg.battery_voltage;
    }
    else {
      resolved.battery_voltage = 0.0
    }

    if (msg.light_control_enabled !== undefined) {
      resolved.light_control_enabled = msg.light_control_enabled;
    }
    else {
      resolved.light_control_enabled = false
    }

    if (msg.front_light_state !== undefined) {
      resolved.front_light_state = light_state.Resolve(msg.front_light_state)
    }
    else {
      resolved.front_light_state = new light_state()
    }

    if (msg.rear_light_state !== undefined) {
      resolved.rear_light_state = light_state.Resolve(msg.rear_light_state)
    }
    else {
      resolved.rear_light_state = new light_state()
    }

    if (msg.motor_num !== undefined) {
      resolved.motor_num = msg.motor_num;
    }
    else {
      resolved.motor_num = 0
    }

    if (msg.motors !== undefined) {
      resolved.motors = new Array(msg.motors.length);
      for (let i = 0; i < resolved.motors.length; ++i) {
        resolved.motors[i] = motor_state.Resolve(msg.motors[i]);
      }
    }
    else {
      resolved.motors = []
    }

    if (msg.motion !== undefined) {
      resolved.motion = motion_state.Resolve(msg.motion)
    }
    else {
      resolved.motion = new motion_state()
    }

    return resolved;
    }
};

module.exports = car_msg;
