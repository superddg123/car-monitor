; Auto-generated. Do not edit!


(cl:in-package car_scout-msg)


;//! \htmlinclude light_state.msg.html

(cl:defclass <light_state> (roslisp-msg-protocol:ros-message)
  ((mode
    :reader mode
    :initarg :mode
    :type cl:integer
    :initform 0))
)

(cl:defclass light_state (<light_state>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <light_state>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'light_state)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name car_scout-msg:<light_state> is deprecated: use car_scout-msg:light_state instead.")))

(cl:ensure-generic-function 'mode-val :lambda-list '(m))
(cl:defmethod mode-val ((m <light_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader car_scout-msg:mode-val is deprecated.  Use car_scout-msg:mode instead.")
  (mode m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <light_state>) ostream)
  "Serializes a message object of type '<light_state>"
  (cl:let* ((signed (cl:slot-value msg 'mode)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <light_state>) istream)
  "Deserializes a message object of type '<light_state>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'mode) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<light_state>)))
  "Returns string type for a message object of type '<light_state>"
  "car_scout/light_state")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'light_state)))
  "Returns string type for a message object of type 'light_state"
  "car_scout/light_state")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<light_state>)))
  "Returns md5sum for a message object of type '<light_state>"
  "ff63f6ea3c3e9b7504b2cb3ee0a09d92")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'light_state)))
  "Returns md5sum for a message object of type 'light_state"
  "ff63f6ea3c3e9b7504b2cb3ee0a09d92")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<light_state>)))
  "Returns full string definition for message of type '<light_state>"
  (cl:format cl:nil "int32 mode~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'light_state)))
  "Returns full string definition for message of type 'light_state"
  (cl:format cl:nil "int32 mode~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <light_state>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <light_state>))
  "Converts a ROS message object to a list"
  (cl:list 'light_state
    (cl:cons ':mode (mode msg))
))
