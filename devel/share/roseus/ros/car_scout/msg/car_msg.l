;; Auto-generated. Do not edit!


(when (boundp 'car_scout::car_msg)
  (if (not (find-package "CAR_SCOUT"))
    (make-package "CAR_SCOUT"))
  (shadow 'car_msg (find-package "CAR_SCOUT")))
(unless (find-package "CAR_SCOUT::CAR_MSG")
  (make-package "CAR_SCOUT::CAR_MSG"))

(in-package "ROS")
;;//! \htmlinclude car_msg.msg.html


(defclass car_scout::car_msg
  :super ros::object
  :slots (_stamp _control_mode _battery_voltage _light_control_enabled _front_light_state _rear_light_state _motor_num _motors _motion ))

(defmethod car_scout::car_msg
  (:init
   (&key
    ((:stamp __stamp) (instance ros::time :init))
    ((:control_mode __control_mode) 0)
    ((:battery_voltage __battery_voltage) 0.0)
    ((:light_control_enabled __light_control_enabled) nil)
    ((:front_light_state __front_light_state) (instance car_scout::light_state :init))
    ((:rear_light_state __rear_light_state) (instance car_scout::light_state :init))
    ((:motor_num __motor_num) 0)
    ((:motors __motors) (let (r) (dotimes (i 0) (push (instance car_scout::motor_state :init) r)) r))
    ((:motion __motion) (instance car_scout::motion_state :init))
    )
   (send-super :init)
   (setq _stamp __stamp)
   (setq _control_mode (round __control_mode))
   (setq _battery_voltage (float __battery_voltage))
   (setq _light_control_enabled __light_control_enabled)
   (setq _front_light_state __front_light_state)
   (setq _rear_light_state __rear_light_state)
   (setq _motor_num (round __motor_num))
   (setq _motors __motors)
   (setq _motion __motion)
   self)
  (:stamp
   (&optional __stamp)
   (if __stamp (setq _stamp __stamp)) _stamp)
  (:control_mode
   (&optional __control_mode)
   (if __control_mode (setq _control_mode __control_mode)) _control_mode)
  (:battery_voltage
   (&optional __battery_voltage)
   (if __battery_voltage (setq _battery_voltage __battery_voltage)) _battery_voltage)
  (:light_control_enabled
   (&optional __light_control_enabled)
   (if __light_control_enabled (setq _light_control_enabled __light_control_enabled)) _light_control_enabled)
  (:front_light_state
   (&rest __front_light_state)
   (if (keywordp (car __front_light_state))
       (send* _front_light_state __front_light_state)
     (progn
       (if __front_light_state (setq _front_light_state (car __front_light_state)))
       _front_light_state)))
  (:rear_light_state
   (&rest __rear_light_state)
   (if (keywordp (car __rear_light_state))
       (send* _rear_light_state __rear_light_state)
     (progn
       (if __rear_light_state (setq _rear_light_state (car __rear_light_state)))
       _rear_light_state)))
  (:motor_num
   (&optional __motor_num)
   (if __motor_num (setq _motor_num __motor_num)) _motor_num)
  (:motors
   (&rest __motors)
   (if (keywordp (car __motors))
       (send* _motors __motors)
     (progn
       (if __motors (setq _motors (car __motors)))
       _motors)))
  (:motion
   (&rest __motion)
   (if (keywordp (car __motion))
       (send* _motion __motion)
     (progn
       (if __motion (setq _motion (car __motion)))
       _motion)))
  (:serialization-length
   ()
   (+
    ;; time _stamp
    8
    ;; int32 _control_mode
    4
    ;; float64 _battery_voltage
    8
    ;; bool _light_control_enabled
    1
    ;; car_scout/light_state _front_light_state
    (send _front_light_state :serialization-length)
    ;; car_scout/light_state _rear_light_state
    (send _rear_light_state :serialization-length)
    ;; int32 _motor_num
    4
    ;; car_scout/motor_state[] _motors
    (apply #'+ (send-all _motors :serialization-length)) 4
    ;; car_scout/motion_state _motion
    (send _motion :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; time _stamp
       (write-long (send _stamp :sec) s) (write-long (send _stamp :nsec) s)
     ;; int32 _control_mode
       (write-long _control_mode s)
     ;; float64 _battery_voltage
       (sys::poke _battery_voltage (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; bool _light_control_enabled
       (if _light_control_enabled (write-byte -1 s) (write-byte 0 s))
     ;; car_scout/light_state _front_light_state
       (send _front_light_state :serialize s)
     ;; car_scout/light_state _rear_light_state
       (send _rear_light_state :serialize s)
     ;; int32 _motor_num
       (write-long _motor_num s)
     ;; car_scout/motor_state[] _motors
     (write-long (length _motors) s)
     (dolist (elem _motors)
       (send elem :serialize s)
       )
     ;; car_scout/motion_state _motion
       (send _motion :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; time _stamp
     (send _stamp :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _stamp :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _control_mode
     (setq _control_mode (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; float64 _battery_voltage
     (setq _battery_voltage (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; bool _light_control_enabled
     (setq _light_control_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; car_scout/light_state _front_light_state
     (send _front_light_state :deserialize buf ptr-) (incf ptr- (send _front_light_state :serialization-length))
   ;; car_scout/light_state _rear_light_state
     (send _rear_light_state :deserialize buf ptr-) (incf ptr- (send _rear_light_state :serialization-length))
   ;; int32 _motor_num
     (setq _motor_num (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; car_scout/motor_state[] _motors
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _motors (let (r) (dotimes (i n) (push (instance car_scout::motor_state :init) r)) r))
     (dolist (elem- _motors)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; car_scout/motion_state _motion
     (send _motion :deserialize buf ptr-) (incf ptr- (send _motion :serialization-length))
   ;;
   self)
  )

(setf (get car_scout::car_msg :md5sum-) "e9d4a19a36701fd76b3d661468664a5a")
(setf (get car_scout::car_msg :datatype-) "car_scout/car_msg")
(setf (get car_scout::car_msg :definition-)
      "time stamp
int32 control_mode
float64 battery_voltage
bool light_control_enabled

light_state front_light_state
light_state rear_light_state

int32 motor_num
motor_state[] motors

motion_state motion

================================================================================
MSG: car_scout/light_state
int32 mode
================================================================================
MSG: car_scout/motor_state
float64 motor_current 
float64 motor_rpm   
int32 motor_pulses   
float64 motor_temperature   

float64 driver_voltage   
float64 driver_temperature   
int32 driver_state   
================================================================================
MSG: car_scout/motion_state
  float64 linear_velocity
  float64 angular_velocity 
")



(provide :car_scout/car_msg "e9d4a19a36701fd76b3d661468664a5a")


