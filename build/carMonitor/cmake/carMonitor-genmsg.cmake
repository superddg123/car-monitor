# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "carMonitor: 4 messages, 0 services")

set(MSG_I_FLAGS "-IcarMonitor:/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(carMonitor_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg" NAME_WE)
add_custom_target(_carMonitor_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "carMonitor" "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg" ""
)

get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg" NAME_WE)
add_custom_target(_carMonitor_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "carMonitor" "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg" ""
)

get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg" NAME_WE)
add_custom_target(_carMonitor_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "carMonitor" "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg" ""
)

get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg" NAME_WE)
add_custom_target(_carMonitor_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "carMonitor" "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg" "carMonitor/motion_state:carMonitor/light_state:carMonitor/motor_state"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/carMonitor
)
_generate_msg_cpp(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/carMonitor
)
_generate_msg_cpp(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/carMonitor
)
_generate_msg_cpp(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg"
  "${MSG_I_FLAGS}"
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/carMonitor
)

### Generating Services

### Generating Module File
_generate_module_cpp(carMonitor
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/carMonitor
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(carMonitor_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(carMonitor_generate_messages carMonitor_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_cpp _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_cpp _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_cpp _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_cpp _carMonitor_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(carMonitor_gencpp)
add_dependencies(carMonitor_gencpp carMonitor_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS carMonitor_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/carMonitor
)
_generate_msg_eus(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/carMonitor
)
_generate_msg_eus(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/carMonitor
)
_generate_msg_eus(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg"
  "${MSG_I_FLAGS}"
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/carMonitor
)

### Generating Services

### Generating Module File
_generate_module_eus(carMonitor
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/carMonitor
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(carMonitor_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(carMonitor_generate_messages carMonitor_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_eus _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_eus _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_eus _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_eus _carMonitor_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(carMonitor_geneus)
add_dependencies(carMonitor_geneus carMonitor_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS carMonitor_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/carMonitor
)
_generate_msg_lisp(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/carMonitor
)
_generate_msg_lisp(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/carMonitor
)
_generate_msg_lisp(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg"
  "${MSG_I_FLAGS}"
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/carMonitor
)

### Generating Services

### Generating Module File
_generate_module_lisp(carMonitor
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/carMonitor
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(carMonitor_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(carMonitor_generate_messages carMonitor_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_lisp _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_lisp _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_lisp _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_lisp _carMonitor_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(carMonitor_genlisp)
add_dependencies(carMonitor_genlisp carMonitor_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS carMonitor_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/carMonitor
)
_generate_msg_nodejs(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/carMonitor
)
_generate_msg_nodejs(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/carMonitor
)
_generate_msg_nodejs(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg"
  "${MSG_I_FLAGS}"
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/carMonitor
)

### Generating Services

### Generating Module File
_generate_module_nodejs(carMonitor
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/carMonitor
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(carMonitor_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(carMonitor_generate_messages carMonitor_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_nodejs _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_nodejs _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_nodejs _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_nodejs _carMonitor_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(carMonitor_gennodejs)
add_dependencies(carMonitor_gennodejs carMonitor_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS carMonitor_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/carMonitor
)
_generate_msg_py(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/carMonitor
)
_generate_msg_py(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/carMonitor
)
_generate_msg_py(carMonitor
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg"
  "${MSG_I_FLAGS}"
  "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg;/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/carMonitor
)

### Generating Services

### Generating Module File
_generate_module_py(carMonitor
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/carMonitor
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(carMonitor_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(carMonitor_generate_messages carMonitor_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/light_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_py _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motion_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_py _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/motor_state.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_py _carMonitor_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/agilex/work/homework/car_soft_task3_demo/ros_ws/src/carMonitor/msg/car_msg.msg" NAME_WE)
add_dependencies(carMonitor_generate_messages_py _carMonitor_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(carMonitor_genpy)
add_dependencies(carMonitor_genpy carMonitor_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS carMonitor_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/carMonitor)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/carMonitor
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(carMonitor_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/carMonitor)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/carMonitor
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(carMonitor_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/carMonitor)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/carMonitor
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(carMonitor_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/carMonitor)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/carMonitor
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(carMonitor_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/carMonitor)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/carMonitor\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/carMonitor
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(carMonitor_generate_messages_py std_msgs_generate_messages_py)
endif()
