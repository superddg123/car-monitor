/********************************************************************************
** Form generated from reading UI file 'CarMonitor.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CARMONITOR_H
#define UI_CARMONITOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CarMonitor
{
public:
    QWidget *centralwidget;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *car_voltage_lable;
    QLabel *car_light_lable;
    QLabel *car_linear_lable;
    QPushButton *up_pushButton;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QLabel *current1_lable;
    QLabel *rpm1_lable;
    QLabel *temp1_lable;
    QLabel *driverTemp1_lable;
    QLabel *voltage1_lable;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *current2_lable;
    QLabel *rpm2_lable;
    QLabel *temp2_lable;
    QLabel *driverTemp2_lable;
    QLabel *voltage2_lable;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_3;
    QVBoxLayout *verticalLayout_3;
    QLabel *current3_lable;
    QLabel *rpm3_lable;
    QLabel *temp3_lable;
    QLabel *driverTemp3_lable;
    QLabel *voltage3_lable;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_4;
    QVBoxLayout *verticalLayout_4;
    QLabel *current4_lable;
    QLabel *rpm4_lable;
    QLabel *temp4_lable;
    QLabel *driverTemp4_lable;
    QLabel *voltage4_lable;
    QPushButton *left_pushButton;
    QPushButton *right_pushButton;
    QPushButton *down_pushButton;
    QLabel *label_28;
    QLabel *carModl_label;
    QLabel *label;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *CarMonitor)
    {
        if (CarMonitor->objectName().isEmpty())
            CarMonitor->setObjectName(QStringLiteral("CarMonitor"));
        CarMonitor->setWindowModality(Qt::ApplicationModal);
        CarMonitor->resize(800, 604);
        CarMonitor->setStyleSheet(QLatin1String("QMainWindow{\n"
"	background-color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton:pressed{\n"
"	\n"
"	\n"
"	background-color: rgb(115, 210, 22);\n"
"}"));
        centralwidget = new QWidget(CarMonitor);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(40, 140, 41, 51));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/ico/img/\345\205\205\347\224\265.png")));
        label_2->setScaledContents(true);
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(30, 240, 61, 71));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/ico/img/\350\275\246\347\201\257.png")));
        label_3->setScaledContents(true);
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(30, 370, 61, 71));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/ico/img/\351\200\237\345\272\246.png")));
        label_4->setScaledContents(true);
        car_voltage_lable = new QLabel(centralwidget);
        car_voltage_lable->setObjectName(QStringLiteral("car_voltage_lable"));
        car_voltage_lable->setGeometry(QRect(110, 150, 91, 41));
        QFont font;
        font.setFamily(QStringLiteral("Ubuntu Condensed"));
        font.setPointSize(20);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        car_voltage_lable->setFont(font);
        car_light_lable = new QLabel(centralwidget);
        car_light_lable->setObjectName(QStringLiteral("car_light_lable"));
        car_light_lable->setGeometry(QRect(120, 250, 67, 41));
        car_light_lable->setFont(font);
        car_linear_lable = new QLabel(centralwidget);
        car_linear_lable->setObjectName(QStringLiteral("car_linear_lable"));
        car_linear_lable->setGeometry(QRect(110, 390, 161, 41));
        car_linear_lable->setFont(font);
        up_pushButton = new QPushButton(centralwidget);
        up_pushButton->setObjectName(QStringLiteral("up_pushButton"));
        up_pushButton->setGeometry(QRect(470, 200, 89, 71));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/ico/img/\345\220\221\344\270\2124.png"), QSize(), QIcon::Normal, QIcon::Off);
        up_pushButton->setIcon(icon);
        up_pushButton->setIconSize(QSize(48, 48));
        up_pushButton->setAutoRepeat(true);
        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(270, 80, 201, 185));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        current1_lable = new QLabel(groupBox);
        current1_lable->setObjectName(QStringLiteral("current1_lable"));
        QFont font1;
        font1.setFamily(QStringLiteral("Ubuntu Condensed"));
        font1.setBold(true);
        font1.setWeight(75);
        current1_lable->setFont(font1);

        verticalLayout->addWidget(current1_lable);

        rpm1_lable = new QLabel(groupBox);
        rpm1_lable->setObjectName(QStringLiteral("rpm1_lable"));
        rpm1_lable->setFont(font1);

        verticalLayout->addWidget(rpm1_lable);

        temp1_lable = new QLabel(groupBox);
        temp1_lable->setObjectName(QStringLiteral("temp1_lable"));
        temp1_lable->setFont(font1);

        verticalLayout->addWidget(temp1_lable);

        driverTemp1_lable = new QLabel(groupBox);
        driverTemp1_lable->setObjectName(QStringLiteral("driverTemp1_lable"));
        driverTemp1_lable->setFont(font1);

        verticalLayout->addWidget(driverTemp1_lable);

        voltage1_lable = new QLabel(groupBox);
        voltage1_lable->setObjectName(QStringLiteral("voltage1_lable"));
        voltage1_lable->setFont(font1);

        verticalLayout->addWidget(voltage1_lable);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);

        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(560, 74, 201, 191));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        current2_lable = new QLabel(groupBox_2);
        current2_lable->setObjectName(QStringLiteral("current2_lable"));
        current2_lable->setFont(font1);

        verticalLayout_2->addWidget(current2_lable);

        rpm2_lable = new QLabel(groupBox_2);
        rpm2_lable->setObjectName(QStringLiteral("rpm2_lable"));
        rpm2_lable->setFont(font1);

        verticalLayout_2->addWidget(rpm2_lable);

        temp2_lable = new QLabel(groupBox_2);
        temp2_lable->setObjectName(QStringLiteral("temp2_lable"));
        temp2_lable->setFont(font1);

        verticalLayout_2->addWidget(temp2_lable);

        driverTemp2_lable = new QLabel(groupBox_2);
        driverTemp2_lable->setObjectName(QStringLiteral("driverTemp2_lable"));
        driverTemp2_lable->setFont(font1);

        verticalLayout_2->addWidget(driverTemp2_lable);

        voltage2_lable = new QLabel(groupBox_2);
        voltage2_lable->setObjectName(QStringLiteral("voltage2_lable"));
        voltage2_lable->setFont(font1);

        verticalLayout_2->addWidget(voltage2_lable);


        gridLayout_2->addLayout(verticalLayout_2, 0, 0, 1, 1);

        groupBox_3 = new QGroupBox(centralwidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(270, 370, 201, 185));
        gridLayout_3 = new QGridLayout(groupBox_3);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        current3_lable = new QLabel(groupBox_3);
        current3_lable->setObjectName(QStringLiteral("current3_lable"));
        current3_lable->setFont(font1);

        verticalLayout_3->addWidget(current3_lable);

        rpm3_lable = new QLabel(groupBox_3);
        rpm3_lable->setObjectName(QStringLiteral("rpm3_lable"));
        rpm3_lable->setFont(font1);

        verticalLayout_3->addWidget(rpm3_lable);

        temp3_lable = new QLabel(groupBox_3);
        temp3_lable->setObjectName(QStringLiteral("temp3_lable"));
        temp3_lable->setFont(font1);

        verticalLayout_3->addWidget(temp3_lable);

        driverTemp3_lable = new QLabel(groupBox_3);
        driverTemp3_lable->setObjectName(QStringLiteral("driverTemp3_lable"));
        driverTemp3_lable->setFont(font1);

        verticalLayout_3->addWidget(driverTemp3_lable);

        voltage3_lable = new QLabel(groupBox_3);
        voltage3_lable->setObjectName(QStringLiteral("voltage3_lable"));
        voltage3_lable->setFont(font1);

        verticalLayout_3->addWidget(voltage3_lable);


        gridLayout_3->addLayout(verticalLayout_3, 0, 0, 1, 1);

        groupBox_4 = new QGroupBox(centralwidget);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setGeometry(QRect(560, 370, 201, 185));
        gridLayout_4 = new QGridLayout(groupBox_4);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        current4_lable = new QLabel(groupBox_4);
        current4_lable->setObjectName(QStringLiteral("current4_lable"));
        current4_lable->setFont(font1);

        verticalLayout_4->addWidget(current4_lable);

        rpm4_lable = new QLabel(groupBox_4);
        rpm4_lable->setObjectName(QStringLiteral("rpm4_lable"));
        rpm4_lable->setFont(font1);

        verticalLayout_4->addWidget(rpm4_lable);

        temp4_lable = new QLabel(groupBox_4);
        temp4_lable->setObjectName(QStringLiteral("temp4_lable"));
        temp4_lable->setFont(font1);

        verticalLayout_4->addWidget(temp4_lable);

        driverTemp4_lable = new QLabel(groupBox_4);
        driverTemp4_lable->setObjectName(QStringLiteral("driverTemp4_lable"));
        driverTemp4_lable->setFont(font1);

        verticalLayout_4->addWidget(driverTemp4_lable);

        voltage4_lable = new QLabel(groupBox_4);
        voltage4_lable->setObjectName(QStringLiteral("voltage4_lable"));
        voltage4_lable->setFont(font1);

        verticalLayout_4->addWidget(voltage4_lable);


        gridLayout_4->addLayout(verticalLayout_4, 0, 0, 1, 1);

        left_pushButton = new QPushButton(centralwidget);
        left_pushButton->setObjectName(QStringLiteral("left_pushButton"));
        left_pushButton->setGeometry(QRect(400, 270, 71, 89));
        left_pushButton->setStyleSheet(QStringLiteral(""));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/ico/img/\345\220\221\345\267\246.png"), QSize(), QIcon::Normal, QIcon::Off);
        left_pushButton->setIcon(icon1);
        left_pushButton->setIconSize(QSize(48, 48));
        left_pushButton->setAutoRepeat(true);
        right_pushButton = new QPushButton(centralwidget);
        right_pushButton->setObjectName(QStringLiteral("right_pushButton"));
        right_pushButton->setGeometry(QRect(560, 270, 71, 89));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/ico/img/\345\220\221\345\217\263.png"), QSize(), QIcon::Normal, QIcon::Off);
        right_pushButton->setIcon(icon2);
        right_pushButton->setIconSize(QSize(48, 48));
        right_pushButton->setAutoRepeat(true);
        down_pushButton = new QPushButton(centralwidget);
        down_pushButton->setObjectName(QStringLiteral("down_pushButton"));
        down_pushButton->setGeometry(QRect(470, 360, 89, 71));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/ico/img/\345\220\221\344\270\2134.png"), QSize(), QIcon::Normal, QIcon::Off);
        down_pushButton->setIcon(icon3);
        down_pushButton->setIconSize(QSize(48, 48));
        down_pushButton->setAutoRepeat(true);
        label_28 = new QLabel(centralwidget);
        label_28->setObjectName(QStringLiteral("label_28"));
        label_28->setGeometry(QRect(260, 10, 281, 41));
        QFont font2;
        font2.setFamily(QStringLiteral("Ubuntu Condensed"));
        font2.setPointSize(20);
        font2.setBold(true);
        font2.setItalic(false);
        font2.setWeight(75);
        label_28->setFont(font2);
        label_28->setStyleSheet(QLatin1String("background-color: rgb(203, 229, 230);\n"
"color:rgb(72, 154, 209);\n"
"border-radius:10px;\n"
""));
        label_28->setAlignment(Qt::AlignCenter);
        carModl_label = new QLabel(centralwidget);
        carModl_label->setObjectName(QStringLiteral("carModl_label"));
        carModl_label->setGeometry(QRect(30, 10, 161, 121));
        carModl_label->setPixmap(QPixmap(QString::fromUtf8(":/ico/img/carModel.png")));
        carModl_label->setScaledContents(true);
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(750, 0, 67, 17));
        CarMonitor->setCentralWidget(centralwidget);
        menubar = new QMenuBar(CarMonitor);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 28));
        CarMonitor->setMenuBar(menubar);
        statusbar = new QStatusBar(CarMonitor);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        CarMonitor->setStatusBar(statusbar);

        retranslateUi(CarMonitor);

        QMetaObject::connectSlotsByName(CarMonitor);
    } // setupUi

    void retranslateUi(QMainWindow *CarMonitor)
    {
        CarMonitor->setWindowTitle(QApplication::translate("CarMonitor", "CarMonitor", Q_NULLPTR));
        label_2->setText(QString());
        label_3->setText(QString());
        label_4->setText(QString());
        car_voltage_lable->setText(QApplication::translate("CarMonitor", "24.5V", Q_NULLPTR));
        car_light_lable->setText(QApplication::translate("CarMonitor", "\345\205\263", Q_NULLPTR));
        car_linear_lable->setText(QApplication::translate("CarMonitor", "0.2 m/s", Q_NULLPTR));
        up_pushButton->setText(QString());
        groupBox->setTitle(QApplication::translate("CarMonitor", "\347\224\265\346\234\2721", Q_NULLPTR));
        current1_lable->setText(QApplication::translate("CarMonitor", "\347\224\265\346\265\201\357\274\232", Q_NULLPTR));
        rpm1_lable->setText(QApplication::translate("CarMonitor", "\350\275\254\351\200\237\357\274\232", Q_NULLPTR));
        temp1_lable->setText(QApplication::translate("CarMonitor", "\346\270\251\345\272\246:", Q_NULLPTR));
        driverTemp1_lable->setText(QApplication::translate("CarMonitor", "\351\251\261\345\212\250\345\231\250\346\270\251\345\272\246:", Q_NULLPTR));
        voltage1_lable->setText(QApplication::translate("CarMonitor", "\351\251\261\345\212\250\345\231\250\347\224\265\345\216\213\357\274\232", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("CarMonitor", "\347\224\265\346\234\2722", Q_NULLPTR));
        current2_lable->setText(QApplication::translate("CarMonitor", "\347\224\265\346\265\201\357\274\232", Q_NULLPTR));
        rpm2_lable->setText(QApplication::translate("CarMonitor", "\350\275\254\351\200\237\357\274\232", Q_NULLPTR));
        temp2_lable->setText(QApplication::translate("CarMonitor", "\346\270\251\345\272\246:", Q_NULLPTR));
        driverTemp2_lable->setText(QApplication::translate("CarMonitor", "\351\251\261\345\212\250\345\231\250\346\270\251\345\272\246:", Q_NULLPTR));
        voltage2_lable->setText(QApplication::translate("CarMonitor", "\351\251\261\345\212\250\345\231\250\347\224\265\345\216\213\357\274\232", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("CarMonitor", "\347\224\265\346\234\2723", Q_NULLPTR));
        current3_lable->setText(QApplication::translate("CarMonitor", "\347\224\265\346\265\201\357\274\232", Q_NULLPTR));
        rpm3_lable->setText(QApplication::translate("CarMonitor", "\350\275\254\351\200\237\357\274\232", Q_NULLPTR));
        temp3_lable->setText(QApplication::translate("CarMonitor", "\346\270\251\345\272\246:", Q_NULLPTR));
        driverTemp3_lable->setText(QApplication::translate("CarMonitor", "\351\251\261\345\212\250\345\231\250\346\270\251\345\272\246:", Q_NULLPTR));
        voltage3_lable->setText(QApplication::translate("CarMonitor", "\351\251\261\345\212\250\345\231\250\347\224\265\345\216\213\357\274\232", Q_NULLPTR));
        groupBox_4->setTitle(QApplication::translate("CarMonitor", "\347\224\265\346\234\2724", Q_NULLPTR));
        current4_lable->setText(QApplication::translate("CarMonitor", "\347\224\265\346\265\201\357\274\232", Q_NULLPTR));
        rpm4_lable->setText(QApplication::translate("CarMonitor", "\350\275\254\351\200\237\357\274\232", Q_NULLPTR));
        temp4_lable->setText(QApplication::translate("CarMonitor", "\346\270\251\345\272\246:", Q_NULLPTR));
        driverTemp4_lable->setText(QApplication::translate("CarMonitor", "\351\251\261\345\212\250\345\231\250\346\270\251\345\272\246:", Q_NULLPTR));
        voltage4_lable->setText(QApplication::translate("CarMonitor", "\351\251\261\345\212\250\345\231\250\347\224\265\345\216\213\357\274\232", Q_NULLPTR));
        left_pushButton->setText(QString());
        right_pushButton->setText(QString());
        down_pushButton->setText(QString());
        label_28->setText(QApplication::translate("CarMonitor", "\346\235\276\347\201\265\345\260\217\350\275\246\347\212\266\346\200\201\346\243\200\346\265\213", Q_NULLPTR));
        carModl_label->setText(QString());
        label->setText(QApplication::translate("CarMonitor", "By xc", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CarMonitor: public Ui_CarMonitor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CARMONITOR_H
